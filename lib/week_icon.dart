import 'package:flutter/material.dart';

class WeekIcon extends StatelessWidget {
  final double width;
  final double height;
  final String value;
  final Color background;
  final Color color;
  WeekIcon({
    this.value,
    this.width,
    this.height,
    this.background,
    this.color,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        width: this.width,
        height: this.height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(this.height),
          color: this.background,
        ),
        alignment: Alignment.center,
        child: Text(this.value, style: TextStyle(color: this.color)),
      ),
    );
  }
}
