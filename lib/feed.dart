import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/progress_bar_dart.dart';
import 'package:hello_world/week_icon.dart';

class Feed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Transform(
          transform: Matrix4.identity()
            ..setEntry(3, 2, 0.001)
            ..translate(1.0, 1.0, 0.1),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF00DCB4), Color(0xFF0095CB)],
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 45, bottom: 45),
                  child: Image.asset('assets/logo_white.png', width: 155),
                ),
                Text(
                  "Hello, James",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 27.0,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  "It is nice to see you again",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white.withOpacity(0.65),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 47.0),
                  child: Text(
                    "2 days streak",
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 21, bottom: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Mo",
                          color: Color(0xFF00D5B6),
                          background: Colors.white,
                        ),
                      ),
                      Container(
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Tu",
                          color: Color(0xFF00D5B6),
                          background: Colors.white,
                        ),
                      ),
                      Container(
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "We",
                          color: Color(0xFF00D5B6),
                          background: Colors.white,
                        ),
                      ),
                      Container(
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Th",
                          color: Colors.white,
                          background: Color(0xFF00E4A2),
                        ),
                      ),
                      DottedBorder(
                        borderType: BorderType.Circle,
                        color: Colors.white,
                        dashPattern: [4],
                        strokeWidth: 0.5,
                        padding: EdgeInsets.zero,
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Fr",
                          color: Colors.white,
                          background: Colors.transparent,
                        ),
                      ),
                      DottedBorder(
                        borderType: BorderType.Circle,
                        color: Colors.white,
                        dashPattern: [4],
                        strokeWidth: 0.5,
                        padding: EdgeInsets.zero,
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Sa",
                          color: Colors.white,
                          background: Colors.transparent,
                        ),
                      ),
                      DottedBorder(
                        borderType: BorderType.Circle,
                        color: Colors.white,
                        dashPattern: [4],
                        strokeWidth: 0.5,
                        padding: EdgeInsets.zero,
                        child: WeekIcon(
                          width: 39,
                          height: 39,
                          value: "Su",
                          color: Colors.white,
                          background: Colors.transparent,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25),
            ),
          ),
          transform: Matrix4.translationValues(0.0, -25.0, 0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 23, bottom: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 46,
                            height: 46,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(46),
                              color: Color(0xFF00E4A3),
                            ),
                            child: Icon(Icons.check, color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 13),
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              "Add your first subject",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Color(0xFF41CCAE)),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 46,
                            height: 46,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(46),
                              color: Color(0xFF00E4A3),
                            ),
                            child: Icon(Icons.check, color: Colors.white),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 13),
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              "Add flashcards",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Color(0xFF41CCAE)),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 46,
                            height: 46,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(46),
                              color: Color(0xFFF3F9FC),
                            ),
                            child:
                                Icon(Icons.computer, color: Color(0xFF97B5BD)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 13),
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Text(
                              "Try Web App",
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Color(0xFFC0CCD5)),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: ProgressBar(
                  background: Color(0xFFEEF3F7),
                  color: Color(0xFF00EAC6),
                  value: 3,
                  total: 6,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
