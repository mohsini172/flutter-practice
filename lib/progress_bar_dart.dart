import 'package:flutter/material.dart';

class ProgressBar extends StatelessWidget {
  final double total;
  final double value;
  final Color background;
  final Color color;
  ProgressBar({
    this.total = 100,
    this.value,
    this.background,
    this.color,
  });
  @override
  Widget build(BuildContext context) {
    var percentage = this.value / this.total;
    return Container(
      color: this.background,
      child: FractionallySizedBox(
        widthFactor: 1.0,
        child: FractionallySizedBox(
          alignment: Alignment.topLeft,
          widthFactor: percentage,
          child: Container(
            height: 5,
            decoration: BoxDecoration(color: this.color),
          ),
        ),
      ),
    );
  }
}
